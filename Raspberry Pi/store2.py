#------------------------------------------
#--- Author: Pradeep Singh
#--- Date: 20th January 2017
#--- Version: 1.0
#--- Python Ver: 2.7
#--- Details At: https://iotbytes.wordpress.com/store-mqtt-data-from-sensors-into-sql-database/
#------------------------------------------


import json
import sqlite3
import mysql.connector
import hashlib
import datetime
import paho.mqtt.client as mqtt
import random, threading, json
import requests
from multiprocessing import Process
from esp_update import func


from datetime import datetime
from Block import Block
MQTT_Broker = "192.168.43.57"
MQTT_Port = 1883
Keep_Alive_Interval = 45000
MQTT_Topic_Humidity = "/hospital_esp_number"

def on_connect(client, userdata, rc):
	if rc != 0:
		pass
		print "Unable to connect to MQTT Broker..."
	else:
		print "Connected with MQTT Broker: " + str(MQTT_Broker)

def on_publish(client, userdata, mid):
	pass
		
def on_disconnect(client, userdata, rc):
	if rc !=0:
		pass	
	
def publish_To_Topic(topic, message):
	mqttc.publish(topic,message)
	print ("Published: " + str(message) + " " + "on MQTT Topic: " + str(topic))
	print ""

def publish_Fake_Sensor_Values_to_MQTT(nn,cbb):
	#threading.Timer(3.0, publish_Fake_Sensor_Values_to_MQTT(nn,cbb)).start()
	"""Humidity_Data = {}
	#Humidity_Data['Sensor_ID'] = "Dummy-1"
	Humidity_Data['Date'] = (datetime.today()).strftime("%d-%b-%Y %H:%M:%S:%f")
	Humidity_Data['Humidity'] = Humidity_Fake_Value
	humidity_json_data = json.dumps(Humidity_Data)

	print "Publishing fake Humidity Value: " + str(Humidity_Fake_Value) + "..."
	publish_To_Topic (MQTT_Topic_Humidity, humidity_json_data)
	toggle = 1
	"""
	mqttc.publish("/publish","I AM "+nn+" publishing this data at: "+cbb)
DB_Name =  "IoT.db"	
mqttc = mqtt.Client()
mqttc.on_connect = on_connect
mqttc.on_disconnect = on_disconnect
mqttc.on_publish = on_publish
mqttc.connect(MQTT_Broker, int(MQTT_Port), int(Keep_Alive_Interval))
class DatabaseManager():
	def __init__(self):
		self.conn = sqlite3.connect(DB_Name)
		self.conn.execute('pragma foreign_keys = on')
		self.conn.commit()
		self.cur = self.conn.cursor()
		
	def add_del_update_db_record(self, sql_query, args=()):
		self.cur.execute(sql_query, args)
		self.conn.commit()
		return

	def __del__(self):
		self.cur.close()
		self.conn.close()

#===============================================================
# Functions to push Sensor Data into Database


def sendReq(i,b1):
    try:
        r = requests.post(i, data={'a': b1.previous_block_hash, 'b': b1.data, 'c': b1.timestamp,"d":b1.hash})
        print(r.status_code, r.reason,r.content)
    except:
        print('Error')


# Function to save Temperature to DB Table
def DHT22_Humidity_Data_Handler(jsonData,topic):
	#Parse Data
        
        print 'in func'
        mydb = mysql.connector.connect(
          host="localhost",
          user="root",
          passwd="root",
          database="saline"
        )
        mycursor = mydb.cursor()
        print 'cursor'
        json_Dict = json.loads(jsonData)
        esp=json_Dict['id']
        print esp
        mycursor.execute("select id from esp where topic='"+esp+"'")
        espid=mycursor.fetchone()[0]
        print espid
        print 'going for next step'
        
        """
        espid=1
        mycursor.execute("select hash from hash where id=(select max(id) from hash) and esp_id="+str(espid))
        abc=mycursor.fetchone()[0]
        print(abc)
        print(mycursor)
        """

        
        mycursor.execute("select hash from hash where id=(select max(id) from hash) and esp_id="+str(espid))
        previous_hash=mycursor.fetchone()[0]
        print previous_hash
        
        print 'DHT22_Humidity_Data_Handler'
	
	nn = json_Dict['status']
	cbb=str((datetime.today()).strftime("%d-%b-%Y %H:%M:%S:%f"))
	print nn
	sql="insert into hash (data,hash,esp_id) values(%s,%s,%s)"
	b=Block(previous_hash,nn,datetime.now())
	print b.get_hash()
	val=[nn,b.get_hash(),espid]
	val=tuple(val)
	print val
	mycursor.execute(sql,val)
	mydb.commit()
	print 'commit'
	#Push into DB Table
	dbObj = DatabaseManager()
	print 'pushing'
	dbObj.add_del_update_db_record("insert into table2 (name,curdate) values (?,?)",[nn,cbb])
	del dbObj
	print "Inserted ESP Data into Database."
	publish_Fake_Sensor_Values_to_MQTT(nn,cbb)
	b1=b
	
	func(esp.split("_")[-1],nn)
	
	
	file=open('myblocks.txt')
        for i in file:
            address=i
        add=address.split(",")
        threadList=[]
        for i in add:
            t = Process(target=sendReq,args=(i,b1))
            threadList.append(t)
            t.start()
        time.sleep(1)
        for i in threadList:
            i.terminate()
        
	print "data published successfully"
	

        
# Function to save Humidity to DB Table



#===============================================================
# Master Function to Select DB Funtion based on MQTT Topic

def sensor_Data_Handler(Topic, jsonData):
    print "reached this checkpoint"
    #if Topic == "/abc":
    DHT22_Humidity_Data_Handler(jsonData,Topic)
    print 'function ended'

#===============================================================
