#include <ESP8266WiFi.h>

#include <PubSubClient.h>

#include "HX711.h"  //You must have this library in your arduino library folder
 
#define DOUT  16

#define CLK  5
 
HX711 scale(DOUT, CLK);
 
//Change this calibration factor as per your load cell once it is found you many need to vary it in thousands
float calibration_factor = -2220;


// Replace the next variables with your SSID/Password combination
const char* ssid = "What the hell??";
const char* password = "nothingboss";

// Add your MQTT Broker IP address, example:
//const char* mqtt_server = "192.168.1.144";
const char* mqtt_server = "192.168.137.247";

int st=-1;

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;

void setup() {
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
    Serial.begin(9600);
}

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* message, unsigned int length) {
  Serial.print("Message arrived on topic: ");
  Serial.print(topic);
  Serial.print(". Message: ");
  String messageTemp;
 
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
    } 
    delay(500);
  }
}
void loop() {
  delay(200);

    scale.set_scale(calibration_factor); //Adjust to this calibration factor

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
float x = scale.get_units();
if(x<0) x=-x;

// Experimentally we found the dependency between wieght and PWM input

float weight=x*2.31+28;

if(unit>=480 && st!=2){
  st=2;
  client.publish("/hospital_esp_number", "{\"id\":\"1_1_1\",\"status\":\"10\"}");
}
else if(unit>=240 && unit<=260 && st!=1){
  st=1;
  client.publish("/hospital_esp_number",  "{\"id\":\"1_1_1\",\"status\":\"01\"}");
}
else if(unit<=60 && st!=0){
  st=0;
  client.publish("/hospital_esp_number",  "{\"id\":\"1_1_1\",\"status\":\"00\"}");
}
}
